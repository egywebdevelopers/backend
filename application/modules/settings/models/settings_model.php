<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Settings_model extends CI_Model {
    
    public function __construct()
    {
        parent::__construct();
    }

    public function update_settings($name, $lang_no, $languages, $copy_rights, $logo = '')
    {
        $this->db->where('subject', 'site_name');
        $this->db->update('settings', array('value' => $name));

        $this->db->where('subject', 'languages_no');
        $this->db->update('settings', array('value' => $lang_no));

        $this->db->where('subject', 'languages');
        $this->db->update('settings', array('value' => $languages));

        $this->db->where('subject', 'copyright');
        $this->db->update('settings', array('value' => $copy_rights));

        if($logo != '')
        {
            $this->db->where('subject', 'logo');
            $this->db->update('settings', array('value' => $logo));
        }

        if($lang_no > 1)
        {
            $langs = explode(',', $languages);
            unset($langs[0]);
            foreach($langs as $lang)
            {
                //-- adding new attributes for added languages
                //-- pages table
                $this->db->query('ALTER TABLE pages ADD title_' . $lang . ' varchar(256) 
                , ADD brief_' . $lang . ' varchar(512) , ADD content_' . $lang . ' text
                , ADD meta_desc_' . $lang . ' varchar(512), ADD meta_keywords_' . $lang . ' varchar(512)');

                //-- items table
                $this->db->query('ALTER TABLE items ADD title_' . $lang . ' varchar(256) 
                , ADD paragraph_' . $lang . ' text');

                //-- tabs table
                $this->db->query('ALTER TABLE tabs ADD title_' . $lang . ' varchar(256) 
                , ADD content_' . $lang . ' text');

                //-- files table
                $this->db->query('ALTER TABLE files ADD title_' . $lang . ' varchar(256) 
                ,ADD brief_' . $lang . ' varchar(512), ADD content_' . $lang . ' text');
            }
        }

        $this->db->where('subject', 'closed');
        $this->db->update('settings', array('value' => 0));
    }

    public function get_settings()
    {
        $query = $this->db->get('settings');
        return $query->result_array();
    }


    
}


/* End of file users_model.php */
/* Location: ./application/modules/users/models/users_model.php */