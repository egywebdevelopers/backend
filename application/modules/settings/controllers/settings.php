<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Settings extends MX_Controller {

	public function __construct()
    {
        parent::__construct();
		$this->deny->deny_if_logged_out();
		
		$this->load->model("settings_model");
		$this->load->helper("img_processing");
    }
	
	
	public function index()
	{
		$data = array();
		$settings = $this->settings_model->get_settings();
		$data['site_name'] = $settings[4]['value'];
		$data['lang_no'] = $settings[0]['value'];
		$data['languages'] = $settings[1]['value'];
		$data['copy_rights'] = $settings[2]['value'];
		$data['logo'] = $settings[3]['value'];
		if ($settings[5]['value'] == 0 ) redirect(show_404());
		if (isset($_POST["submit"]))
		{
			$name = htmlspecialchars(trim($_POST["name"]));
			$lang_no = $_POST['languages_no'];
			$languages = htmlspecialchars(trim($_POST["languages"]));
			$copy_rights = htmlspecialchars(trim($_POST["copy_rights"]));
			$logo = @$_FILES["logo"]["name"];

			if (empty($name) OR empty($lang_no) OR empty($languages) OR empty($copy_rights))
			{
				$data["status"] = "<p class='error-msg'>Please fill in required fields(*)</p>";
			}
			else
			{
				if ( ! empty($_FILES["logo"]["name"]))
				{
					$tmp_name = $_FILES['logo']['tmp_name']; // getting the temporary file name
					$allowed_exts = array('jpg', 'JPG', 'jpeg', 'JPEG', 'png', 'PNG', 'gif', 'GIF'); // specifying the allowed extensions
					$a = explode('.', $logo);
					$file_ext = strtolower(end($a)); unset($a);
					$path = USER_PHOTOS_PATH; // folder we store the employees photos in

					// Restricting file uploading to image files only
					if ( ! in_array($file_ext, $allowed_exts))
					{
						$data["status"] = "<p class='error-msg'>This file extension is not an image!</p>";
					}
					else
					{
						// Success. Give the picture a unique name and upload it
						$picture_name = time() . "." . $file_ext;
						move_uploaded_file($tmp_name, $path . $picture_name);
						$this->settings_model->update_settings($name, $lang_no, $languages, $copy_rights, $picture_name);
						$data["status"] = "<p class='error-msg'>Settings changed successfully </p>";
					}
				}
				else
				{
					$this->settings_model->update_settings($name, $lang_no, $languages, $copy_rights);
					$data["status"] = "<p class='error-msg'>Settings changed successfully </p>";
				}
			}
		}
		$this->load->view("settings_view", $data);
    }

}


/* End of file users.php */
/* Location: ./application/modules/users/controllers/users.php */