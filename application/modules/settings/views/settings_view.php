<?php $this->load->view('header'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Dashboard
			<small>Settings</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Users</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<!-- Small boxes (Stat box) -->
		<div class="box box-info">
			<div class="box-header with-border">
				<h3 class="box-title">
					<?php if (isset($status)): ?>
						<?= $status; ?>
					<?php endif; ?>
				</h3>
			</div><!-- /.box-header -->
			<!-- form start -->
			<form class="form-horizontal" method="post" enctype="multipart/form-data">
				<div class="box-body">
					<div class="form-group">
						<label for="inputEmail3" class="col-sm-2 control-label">Website Name*</label>
						<div class="col-sm-10">
							<input type="text" name="name" class="form-control" value="<?php if (isset($site_name)) echo htmlspecialchars(trim($site_name)); ?>" placeholder="Website Name" required autofocus />
						</div>
					</div>
					<div class="form-group">
						<label for="inputEmail3" class="col-sm-2 control-label">Languages No*</label>
						<div class="col-sm-10">
							<select name="languages_no" class="form-control" required>
								<option value=""></option>
								<option value="1" <?php if(isset($lang_no) && $lang_no == 1) echo "selected" ?>>1</option>
								<option value="2" <?php if(isset($lang_no) && $lang_no == 2) echo "selected" ?>>2</option>
								<option value="3" <?php if(isset($lang_no) && $lang_no == 3) echo "selected" ?>>3</option>
								<option value="4" <?php if(isset($lang_no) && $lang_no == 4) echo "selected" ?>>4</option>
								<option value="5" <?php if(isset($lang_no) && $lang_no == 5) echo "selected" ?>>5</option>
							</select>
						</div>
					</div>


					<div class="form-group">
						<label for="inputEmail3" class="col-sm-2 control-label">Languages*</label>
						<div class="col-sm-10">
							<input type="text" name="languages" class="form-control" value="<?php if (isset($languages)) echo htmlspecialchars(trim($languages)); ?>" placeholder="like EN,AR etc."  required autofocus />
						</div>
					</div>

					<div class="form-group">
						<label for="inputEmail3" class="col-sm-2 control-label">Copyrights*</label>
						<div class="col-sm-10">
							<input type="text" name="copy_rights" class="form-control" value="<?php if (isset($copy_rights)) echo htmlspecialchars(trim($copy_rights)); ?>" placeholder="Copy Rights" required autofocus />
						</div>
					</div>

					<div class="form-group">
						<label for="inputEmail3" class="col-sm-2 control-label">Logo</label>
						<div class="col-sm-10">
							<?php if(isset($logo)): ?>
							<img src="<?= USER_PHOTOS.$logo ?>">
							<?php endif; ?>
							<input type="file" name="logo" class="form-control" autofocus />
						</div>
					</div>


				</div><!-- /.box-body -->
				<div class="box-footer">
					<button type="submit" name="submit" class="btn btn-info pull-right">Save</button>
				</div><!-- /.box-footer -->
			</form>
		</div>
		<!-- Main row -->


	</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<?php $this->load->view('footer'); ?>
