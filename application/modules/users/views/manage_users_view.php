<?php $this->load->view('header'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Dashboard
			<small>Manage Users</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Users</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<!-- Small boxes (Stat box) -->

		<!-- Main row -->
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title">Backend Users</h3>
						<div class="box-tools">
							<div class="input-group" style="width: 150px;">
								<input type="text" name="table_search" class="form-control input-sm pull-right" placeholder="Search">
								<div class="input-group-btn">
									<button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
								</div>
							</div>
						</div>
					</div><!-- /.box-header -->
					<div class="box-body table-responsive no-padding">
						<table class="table table-hover">
							<tbody><tr>
								<th>ID</th>
								<th>User</th>
								<th>Username</th>
								<th>Mobile</th>
								<th>Email</th>
								<th>Edit</th>
								<th>Delete</th>
							</tr>
							<?php if (isset($users)): ?>
							<?php foreach ($users as $user): ?>
							<tr>
								<td><?= $user["id"]; ?></td>
								<td><?= $user["name"]; ?></td>
								<td><?= $user["username"]; ?></td>
								<td><?= $user["mobile"]; ?></td>
								<td><?= $user["email"]; ?></td>
								<td><a href="<?= site_url() ?>users/edit/<?= $user['id']; ?>" ><button class="btn btn-block btn-primary btn-flat">Edit</button></td>
								<td><a onclick="alertDelete('users/delete/<?= $user['id']; ?>', 'Are you sure you want to delete this user?');" href="javascript:void(null);"><button class="btn btn-block btn-danger btn-flat">Delete</button></a></td>
							</tr>

								<?php endforeach; ?>
							<?php endif; ?>

							</tbody></table>
					</div><!-- /.box-body -->
				</div><!-- /.box -->
			</div>
		</div>
		<?php if (isset($pagination)): ?>

				<?= $pagination; ?>

		<?php endif; ?>
		<!-- Empty modal -->
		<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content"></div>
			</div>
		</div>
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<?php $this->load->view('footer'); ?>
