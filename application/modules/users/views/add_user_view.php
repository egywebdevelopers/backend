<?php $this->load->view('header'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Dashboard
			<small>Add User</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Users</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<!-- Small boxes (Stat box) -->
		<div class="box box-info">
			<div class="box-header with-border">
				<h3 class="box-title">
					<?php if (isset($status)): ?>
						<?= $status; ?>
					<?php endif; ?>
				</h3>
			</div><!-- /.box-header -->
			<!-- form start -->
			<form class="form-horizontal" method="post" enctype="multipart/form-data">
				<div class="box-body">
					<div class="form-group">
						<label for="inputEmail3" class="col-sm-2 control-label">Full name*</label>
						<div class="col-sm-10">
							<input type="text" name="name" class="form-control" value="<?php if (isset($_POST['name'])) echo htmlspecialchars(trim($_POST['name'])); ?>" placeholder="Full Name" required autofocus />
						</div>
					</div>
					<div class="form-group">
						<label for="inputEmail3" class="col-sm-2 control-label">Username*</label>
						<div class="col-sm-10">
							<input type="text" name="username" class="form-control" value="<?php if (isset($_POST['username'])) echo htmlspecialchars(trim($_POST['username'])); ?>" placeholder="User Name" required autofocus />
						</div>
					</div>
					<div class="form-group">
						<label for="inputEmail3" class="col-sm-2 control-label">Password*</label>
						<div class="col-sm-10">
							<input type="password" name="password" class="form-control" placeholder="Password" required autofocus />
						</div>
					</div>
					<div class="form-group">
						<label for="inputEmail3" class="col-sm-2 control-label">Confirm Password*</label>
						<div class="col-sm-10">
							<input type="password" name="confirm_password" class="form-control"  placeholder="Confirm Password" required autofocus />
						</div>
					</div>
					<div class="form-group">
						<label for="inputEmail3" class="col-sm-2 control-label">Mobile</label>
						<div class="col-sm-10">
							<input type="text" name="mobile" class="form-control" value="<?php if (isset($_POST['mobile'])) echo htmlspecialchars(trim($_POST['mobile'])); ?>" placeholder="Mobile"  autofocus />
						</div>
					</div>
					<div class="form-group">
						<label for="inputEmail3" class="col-sm-2 control-label">Email</label>
						<div class="col-sm-10">
							<input type="email" name="email" class="form-control" value="<?php if (isset($_POST['email'])) echo htmlspecialchars(trim($_POST['email'])); ?>" placeholder="Email"  autofocus />
						</div>
					</div>
					<span class="error"></span>
					<div class="col-md-122">
						<div class="image-crop" style="margin-top: 20px;">
							<h4 class="StepTitle">profile picture</h4>
							<div class="bbody">
								<!-- hidden crop params -->
								<input type="hidden" id="x1" name="x1">
								<input type="hidden" id="y1" name="y1">
								<input type="hidden" id="w" name="w" />
								<input type="hidden" id="h" name="h" />
								<h4>Step1: Please select image file</h4>
								<div>
									<input type="file" name="picture" id="image_file" onchange="fileSelectHandler()" />
								</div>
								<div class="step255">
									<h4 style="padding-top: 45px;">Step2: Please select a crop region</h4>
									<img id="preview" />
								</div>
							</div>
						</div>
					</div>

				</div><!-- /.box-body -->
				<div class="box-footer">
					<button type="submit" name="submit" class="btn btn-info pull-right">Add User</button>
				</div><!-- /.box-footer -->
			</form>
		</div>
		<!-- Main row -->


	</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<?php $this->load->view('footer'); ?>
