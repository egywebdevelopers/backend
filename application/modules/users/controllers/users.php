<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends MX_Controller {

	public function __construct()
    {
        parent::__construct();
		$this->deny->deny_if_logged_out();
		
		$this->load->model("users_model");
		$this->load->helper("img_processing");
    }
	
	
	public function index()
	{
		$data = array();

		$current_page = (int) $this->uri->segment(2);
		$per_page = 20;
		$users_count = $this->common_model->get_table_rows_count("users_details");

		$config["base_url"] = site_url() . "users/";
		$config['uri_segment'] = 20;
		$config["total_rows"] = $users_count;
		$config["per_page"] = $per_page;
		$config['full_tag_open'] = "<ul class='pagination'>";
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";

		$this->pagination->initialize($config);

		$users = $this->users_model->get_users($current_page, $per_page);
		if ($users)
		{
			$data["users"] = $users;
			$data["pagination"] = $this->pagination->create_links();
		}

		if (isset($_POST["submit"]))
		{
			$query = htmlspecialchars(trim($_POST["search"]));
			redirect(site_url() . "users/search/$query");
		}

		$this->load->view("manage_users_view", $data);
    }
	
	
	public function search($query = "")
	{
		$authorized = $this->common_model->authorized_to_view_page("manage_users");
		if ($authorized)
		{
			if (empty($query)) redirect(site_url() . "users");
			$query = urldecode($query);
			
			$data = array();
			
			$current_page = (int) $this->uri->segment(4);
			$per_page = 1;
			$users_count = $this->users_model->get_search_rows_count($query);
			
			$config["base_url"] = site_url() . "users/search/$query";
			$config['uri_segment'] = 4;
			$config["total_rows"] = $users_count;
			$config["per_page"] = $per_page;
			$this->pagination->initialize($config);
			
			$users = $this->users_model->search_users($query, $current_page, $per_page);
			if ($users)
			{
				$data["users"] = $users;
				$data["pagination"] = $this->pagination->create_links();
			}
			
			if (isset($_POST["submit"]))
			{
				$query = htmlspecialchars(trim($_POST["search"]));
				redirect(site_url() . "users/search/$query");
			}
			
			$this->load->view("manage_users_view", $data);
		}
	}
	
	
	public function add()
	{
		$data = array();

		if (isset($_POST["submit"]))
		{
			$name = htmlspecialchars(trim($_POST["name"]));
			$picture_name = basename($_FILES["picture"]["name"]);
			$username = htmlspecialchars(trim($_POST["username"]));
			$password = htmlspecialchars(trim($_POST["password"]));
			$confirm_password = htmlspecialchars(trim($_POST["confirm_password"]));
			$mobile = htmlspecialchars(trim($_POST["mobile"]));
			$email = htmlspecialchars(trim($_POST["email"]));
			$w = $_POST["w"];
			$h = $_POST["h"];
			$x1 = $_POST["x1"];
			$y1 = $_POST["y1"];

			if (empty($name) OR empty($username) OR empty($password) OR empty($confirm_password))
			{
				$data["status"] = "<p class='error-msg'>Please fill in required fields(*)</p>";
			}
			elseif ($this->common_model->subject_exists("users_details", "username", $username))
			{
				$data["status"] = "<p class='error-msg'>This username is not available!</p>";
			}
			elseif ($password !== $confirm_password)
			{
				$data["status"] = "<p class='error-msg'>Please make sure that password and confirm password fields are the same!</p>";
			}
			elseif (strlen($password) < 6)
			{
				$data["status"] = "<p class='error-msg'>Password shouldn't be less than 6 characters!</p>";
			}
			elseif ( ! empty($mobile) && ( ! is_numeric($mobile) OR strlen($mobile) != 11))
			{
				$data["status"] = "<p class='error-msg'>This mobile number is not valid!</p>";
			}
			elseif ( ! empty($email) && ( ! filter_var($email, FILTER_VALIDATE_EMAIL)))
			{
				$data["status"] = "<p class='error-msg'>This email address is invalid!</p>";
			}
			else
			{
				if ( ! empty($_FILES["picture"]["name"]))
				{
					$tmp_name = $_FILES['picture']['tmp_name']; // getting the temporary file name
					$allowed_exts = array('jpg', 'JPG', 'jpeg', 'JPEG', 'png', 'PNG', 'gif', 'GIF'); // specifying the allowed extensions
					$a = explode('.', $picture_name);
					$file_ext = strtolower(end($a)); unset($a);
					$path = USER_PHOTOS_PATH; // folder we store the employees photos in

					// Restricting file uploading to image files only
					if ( ! in_array($file_ext, $allowed_exts))
					{
						$data["status"] = "<p class='error-msg'>This file extension is not an image!</p>";
					}
					else
					{
						// Success. Give the picture a unique name and upload it
						$last_id = $this->common_model->get_table_max_id("users_details");
						$picture_name = $last_id + 1 . "_" . time() . "." . $file_ext;
						move_uploaded_file($tmp_name, $path . $picture_name);

						// Now crop then resize the picture
						custom_image_crop($path . $picture_name, $path . $picture_name, $x1, $y1, $w, $h);
						custom_image_resize($path . $picture_name, $path . $picture_name, 250, 250);
					}
				}

				// Insert information into database and log action
				$insert_id = $this->users_model->insert_user($name, $picture_name, $username, $password, $mobile, $email);

				$this->session->set_flashdata("status", "Added successfully.");
				redirect(site_url() . "users");
			}
		}

		$this->load->view("add_user_view", $data);

	}

	
    public function edit($id = "")
	{
		$user = $this->common_model->get_subject_with_token("users_details", "id", $id);
		if (empty($id) OR ! $user) redirect(site_url() . "users");

		$data["user"] = $user;

		if (isset($_POST["info_submit"]))
		{
			$name = htmlspecialchars(trim($_POST["name"]));
			$mobile = htmlspecialchars(trim($_POST["mobile"]));
			$email = htmlspecialchars(trim($_POST["email"]));

			if (empty($name))
			{
				$data["status"] = "<p class='error-msg'>Please fill in required fields(*)</p>";
			}
			elseif ( ! empty($mobile) && ( ! is_numeric($mobile) OR strlen($mobile) != 11))
			{
				$data["status"] = "<p class='error-msg'>Invalid mobile number!</p>";
			}
			elseif ( ! empty($email) && ( ! filter_var($email, FILTER_VALIDATE_EMAIL)))
			{
				$data["status"] = "<p class='error-msg'>Invalid email address!</p>";
			}
			else
			{

				$this->users_model->update_user_info($name, $mobile, $email, $id);

				$this->session->set_flashdata("status", "Info updated successfully!");
				redirect(site_url() . "users");
			}
		}
		elseif (isset($_POST["password_submit"]))
		{
			$password = htmlspecialchars(trim($_POST["password"]));
			$confirm_password = htmlspecialchars(trim($_POST["confirm_password"]));

			if (empty($password) OR empty($confirm_password))
			{
				$data["status"] = "<p class='error-msg'>Please fill in required fields(*)</p>";
			}
			elseif ($password !== $confirm_password)
			{
				$data["status"] = "<p class='error-msg'>Please make sure that password and confirm password fields are the same!</p>";
			}
			elseif (strlen($password) < 6)
			{
				$data["status"] = "<p class='error-msg'>Password shouldn't be less than 6 characters!</p>";
			}
			else
			{
				$this->users_model->update_user_password($password, $id);

				$this->session->set_flashdata("status", "Password changed successfully");
				redirect(site_url() . "users");
			}
		}
		elseif (isset($_POST["picture_submit"]))
		{
			$picture_name = basename($_FILES["picture"]["name"]);
			$w = $_POST["w"];
			$h = $_POST["h"];
			$x1 = $_POST["x1"];
			$y1 = $_POST["y1"];
			$tmp_name = $_FILES['picture']['tmp_name']; // getting the temporary file name
			$allowed_exts = array('jpg', 'JPG', 'jpeg', 'JPEG', 'png', 'PNG', 'gif', 'GIF'); // specifying the allowed extensions
			$a = explode('.', $picture_name);
			$file_ext = strtolower(end($a)); unset($a); // getting the allowed extensions
			$path = USER_PHOTOS_PATH; // folder we store the employees photos in

			if (empty($picture_name))
			{
				$data["status"] = "<p class='error-msg'>You didn't select a picture</p>";
			}
			else
			{
				// Restricting file uploading to image files only
				if ( ! in_array($file_ext, $allowed_exts))
				{
					$data["status"] = "<p class='error-msg'>This file extension is not an image!</p>";
				}
				else
				{
					// Success. First delete the old picture then make new one
					@unlink($path . $user["picture"]);
					$picture_name = $id . "_" . time() . "." . $file_ext;
					move_uploaded_file($tmp_name, $path . $picture_name);

					// Now crop then resize the picture
					custom_image_crop($path . $picture_name, $path . $picture_name, $x1, $y1, $w, $h);
					custom_image_resize($path . $picture_name, $path . $picture_name, 250, 250);

					$this->users_model->update_user_picture($picture_name, $id);

					$this->session->set_flashdata("status", "Picture changed successfully");
					redirect(site_url() . "users");
				}
			}
		}

		$this->load->view("edit_user_view", $data);

	}
    
	
	public function delete($id = "")
    {
		$user = $this->common_model->get_subject_with_token("users_details", "id", $id);
		if (empty($id) OR ! $user) redirect(site_url() . "users");

		$this->common_model->delete_subject("users_details", "id", $id);

		// And if user has a photo delete it from desk as well
		if ( ! empty($user["picture"]))
		{
			$user_photo = USER_PHOTOS_PATH . $user["picture"];
			if (file_exists($user_photo)) unlink($user_photo);
		}

		$this->session->set_flashdata("status", "User deleted successfully.");
		redirect($_SERVER['HTTP_REFERER']);
	}
	
	
	
	
    public function test()
	{
		header("Content-Type: text/html; charset=utf-8");
		
	}
	
}


/* End of file users.php */
/* Location: ./application/modules/users/controllers/users.php */