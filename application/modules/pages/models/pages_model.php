<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class pages_model extends CI_Model {
    
    public function __construct()
    {
        parent::__construct();
    }



    public function insert_page($lang_data, $title, $brief, $content, $meta_desc, $meta_keywords, $form, $toplinks,
                                $sidebar, $menu, $categories, $published, $subof)
    {
        $arr = array('title' => $title, 'brief' => $brief, 'content' => $content, 'meta_desc' => $meta_desc, 'meta_keywords' => $meta_keywords,'form' => $form,
                    'top_links' => $toplinks, 'sidebar' => $sidebar, 'menu' => $menu, 'categories' => $categories, 'published' => $published, 'sub_of_page_id' => $subof);

        $data_arr =  array_merge($arr, $lang_data);
        $this->db->insert('pages', $data_arr);

    }

    public function update_page($id,$lang_data, $title, $brief, $content, $meta_desc, $meta_keywords, $form,
                                $toplinks, $sidebar, $menu, $categories, $published, $subof)
    {
        $arr = array('title' => $title, 'brief' => $brief, 'content' => $content, 'meta_desc' => $meta_desc, 'meta_keywords' => $meta_keywords,'form' => $form,
            'top_links' => $toplinks, 'sidebar' => $sidebar, 'menu' => $menu, 'categories' => $categories, 'published' => $published, 'sub_of_page_id' => $subof);

        $data_arr =  array_merge($arr, $lang_data);
        $this->db->where('ID', $id);
        $this->db->update('pages', $data_arr);
    }
    
    public function get_langs()
    {
        $this->db->where('subject', 'languages');
        $query = $this->db->get('settings');
        return $query->row_array();
    }

    public function get_pages()
    {
        $this->db->select('ID, title');
        $query = $this->db->get('pages');
        return $query->result_array();
    }

    public function get_pages_pagination($offset, $limit)
    {
        $this->db->order_by('ID', 'desc');
        $query = $this->db->get('pages');
        return $query->result_array();
    }

    
}


/* End of file users_model.php */
/* Location: ./application/modules/users/models/users_model.php */