<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pages extends MX_Controller {

	public function __construct()
    {
        parent::__construct();
		$this->deny->deny_if_logged_out();
		
		$this->load->model("pages_model");
		$this->load->helper("img_processing");
    }
	
	
	public function index()
	{
		$data = array();

		$current_page = (int) $this->uri->segment(2);
		$per_page = 20;
		$users_count = $this->common_model->get_table_rows_count("pages");

		$config["base_url"] = site_url() . "users/";
		$config['uri_segment'] = 20;
		$config["total_rows"] = $users_count;
		$config["per_page"] = $per_page;
		$config['full_tag_open'] = "<ul class='pagination'>";
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";

		$this->pagination->initialize($config);

		$pages = $this->pages_model->get_pages_pagination($current_page, $per_page);
		if ($pages)
		{
			$data["pages"] = $pages;
			$data["pagination"] = $this->pagination->create_links();
		}

		if (isset($_POST["submit"]))
		{
			$query = htmlspecialchars(trim($_POST["search"]));
			redirect(site_url() . "users/search/$query");
		}

		$this->load->view("manage_pages_view", $data);
    }
	
	
	public function search($query = "")
	{
		$authorized = $this->common_model->authorized_to_view_page("manage_users");
		if ($authorized)
		{
			if (empty($query)) redirect(site_url() . "users");
			$query = urldecode($query);
			
			$data = array();
			
			$current_page = (int) $this->uri->segment(4);
			$per_page = 1;
			$users_count = $this->users_model->get_search_rows_count($query);
			
			$config["base_url"] = site_url() . "users/search/$query";
			$config['uri_segment'] = 4;
			$config["total_rows"] = $users_count;
			$config["per_page"] = $per_page;
			$this->pagination->initialize($config);
			
			$users = $this->users_model->search_users($query, $current_page, $per_page);
			if ($users)
			{
				$data["users"] = $users;
				$data["pagination"] = $this->pagination->create_links();
			}
			
			if (isset($_POST["submit"]))
			{
				$query = htmlspecialchars(trim($_POST["search"]));
				redirect(site_url() . "users/search/$query");
			}
			
			$this->load->view("manage_users_view", $data);
		}
	}
	
	
	public function add()
	{
		$data = array();
		$langs = $this->pages_model->get_langs();
		$langs_arr = explode(',', $langs['value']);
		unset($langs_arr[0]);
		$data['langs'] = $langs_arr;

		$data['pages'] = $this->pages_model->get_pages();

		if (isset($_POST["submit"]))
		{
			$title = htmlspecialchars(trim($_POST["title"]));
			$brief = htmlspecialchars(trim($_POST["brief"]));
			$content = htmlspecialchars(trim($_POST["content"]));
			$meta_desc = htmlspecialchars(trim($_POST["meta_desc"]));
			$meta_keywords = htmlspecialchars(trim($_POST["meta_keywords"]));
			$form = (isset($_POST['form']) && $_POST['form'] == 1) ? 1 : 0 ;
			$toplinks = (isset($_POST['toplinks']) && $_POST['toplinks'] == 1) ? 1 : 0 ;
			$sidebar = (isset($_POST['sidebar']) && $_POST['sidebar'] == 1) ? 1 : 0 ;
			$menu = (isset($_POST['menu']) && $_POST['menu'] == 1) ? 1 : 0 ;
			$categories = (isset($_POST['categories']) && $_POST['categories'] == 1) ? 1 : 0 ;
			$published = (isset($_POST['published']) && $_POST['published'] == 1) ? 1 : 0 ;
			$subof = $_POST['subof'];
			$lang_data = array();
			foreach ($langs_arr as $lang)
			{
				$lang_data['title_'.$lang] = htmlspecialchars(trim($_POST["title_".$lang]));
				$lang_data['brief_'.$lang] = htmlspecialchars(trim($_POST["brief_".$lang]));
				$lang_data['content_'.$lang] = htmlspecialchars(trim($_POST["content_".$lang]));
				$lang_data['meta_desc_'.$lang] = htmlspecialchars(trim($_POST["meta_desc_".$lang]));
				$lang_data['meta_keywords_'.$lang] = htmlspecialchars(trim($_POST["meta_keywords_".$lang]));
			}

			if (empty($title) OR empty($brief) OR empty($content) OR empty($meta_desc) OR empty($meta_keywords))
			{
				$data["status"] = "<p class='error-msg'>Please fill in required fields(*)</p>";
			}
			else
			{
				// Insert information into database
				$this->pages_model->insert_page($lang_data, $title, $brief, $content, $meta_desc, $meta_keywords, $form, $toplinks,
												$sidebar, $menu, $categories, $published, $subof);

				$this->session->set_flashdata("status", "Added successfully.");
				redirect(site_url() . "pages");
			}
		}

		$this->load->view("add_page_view", $data);

	}

	
    public function edit($id = "")
	{
		$data = array();
		$page = $this->common_model->get_subject_with_token("pages", "id", $id);
		if (empty($id) OR ! $page) redirect(site_url() . "pages");
		$data['page'] = $page;
		$langs = $this->pages_model->get_langs();
		$langs_arr = explode(',', $langs['value']);
		unset($langs_arr[0]);
		$data['langs'] = $langs_arr;

		$data['pages'] = $this->pages_model->get_pages();

		if (isset($_POST["submit"]))
		{
			$title = htmlspecialchars(trim($_POST["title"]));
			$brief = htmlspecialchars(trim($_POST["brief"]));
			$content = htmlspecialchars(trim($_POST["content"]));
			$meta_desc = htmlspecialchars(trim($_POST["meta_desc"]));
			$meta_keywords = htmlspecialchars(trim($_POST["meta_keywords"]));
			$form = (isset($_POST['form']) && $_POST['form'] == 1) ? 1 : 0 ;
			$toplinks = (isset($_POST['toplinks']) && $_POST['toplinks'] == 1) ? 1 : 0 ;
			$sidebar = (isset($_POST['sidebar']) && $_POST['sidebar'] == 1) ? 1 : 0 ;
			$menu = (isset($_POST['menu']) && $_POST['menu'] == 1) ? 1 : 0 ;
			$categories = (isset($_POST['categories']) && $_POST['categories'] == 1) ? 1 : 0 ;
			$published = (isset($_POST['published']) && $_POST['published'] == 1) ? 1 : 0 ;
			$subof = $_POST['subof'];
			$lang_data = array();
			foreach ($langs_arr as $lang)
			{
				$lang_data['title_'.$lang] = htmlspecialchars(trim($_POST["title_".$lang]));
				$lang_data['brief_'.$lang] = htmlspecialchars(trim($_POST["brief_".$lang]));
				$lang_data['content_'.$lang] = htmlspecialchars(trim($_POST["content_".$lang]));
				$lang_data['meta_desc_'.$lang] = htmlspecialchars(trim($_POST["meta_desc_".$lang]));
				$lang_data['meta_keywords_'.$lang] = htmlspecialchars(trim($_POST["meta_keywords_".$lang]));
			}

			if (empty($title) OR empty($brief) OR empty($content) OR empty($meta_desc) OR empty($meta_keywords))
			{
				$data["status"] = "<p class='error-msg'>Please fill in required fields(*)</p>";
			}
			else
			{
				// Insert information into database
				$this->pages_model->update_page($id, $lang_data, $title, $brief, $content, $meta_desc, $meta_keywords, $form, $toplinks,
					$sidebar, $menu, $categories, $published, $subof);

				$this->session->set_flashdata("status", "Saved successfully.");
				redirect(site_url() . "pages");
			}
		}

		$this->load->view("edit_page_view", $data);

	}
    
	
	public function delete($id = "")
    {
		$page = $this->common_model->get_subject_with_token("pages", "id", $id);
		if (empty($id) OR ! $page) redirect(site_url() . "pages");

		$this->common_model->delete_subject("pages", "id", $id);

		$this->session->set_flashdata("status", "Page deleted successfully.");
		redirect($_SERVER['HTTP_REFERER']);
	}

}


/* End of file users.php */
/* Location: ./application/modules/users/controllers/users.php */