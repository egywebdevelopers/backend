<?php $this->load->view('header'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Dashboard
			<small>Manage Pages</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Pages</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<!-- Small boxes (Stat box) -->

		<!-- Main row -->
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title">All Pages</h3>
						<div class="box-tools">
							<div class="input-group" style="width: 150px;">
								<input type="text" name="table_search" class="form-control input-sm pull-right" placeholder="Search">
								<div class="input-group-btn">
									<button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
								</div>
							</div>
						</div>
					</div><!-- /.box-header -->
					<div class="box-body table-responsive no-padding">
						<table class="table table-hover">
							<tbody><tr>
								<th>ID</th>
								<th>Page</th>
								<th>Brief</th>
								<th>Published</th>
								<th>Added at</th>
								<th>Manage Items</th>
								<th>Edit</th>
								<th>Delete</th>
							</tr>
							<?php if (isset($pages)): ?>
							<?php foreach ($pages as $page): ?>
							<tr>
								<td><?= $page["ID"]; ?></td>
								<td><?= $page["title"]; ?></td>
								<td><?= $page["brief"]; ?></td>
								<td><?php if($page["published"] == 1) echo "Yes"; else echo "No"; ?></td>
								<td><?= $page["added_at"]; ?></td>
								<td><a href="<?= site_url() ?>items/page/<?= $page['ID']; ?>" ><button class="btn btn-block btn-warning btn-flat">Manage Items</button></td>
								<td><a href="<?= site_url() ?>pages/edit/<?= $page['ID']; ?>" ><button class="btn btn-block btn-primary btn-flat">Edit</button></td>
								<td><a onclick="alertDelete('pages/delete/<?= $page['ID']; ?>', 'Are you sure you want to delete this page?');" href="javascript:void(null);"><button class="btn btn-block btn-danger btn-flat">Delete</button></a></td>
							</tr>

								<?php endforeach; ?>
							<?php endif; ?>

							</tbody></table>
					</div><!-- /.box-body -->
				</div><!-- /.box -->
			</div>
		</div>
		<?php if (isset($pagination)): ?>

				<?= $pagination; ?>

		<?php endif; ?>
		<!-- Empty modal -->
		<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content"></div>
			</div>
		</div>
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<?php $this->load->view('footer'); ?>
