<?php $this->load->view('header'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Dashboard
			<small>Edit Page</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Pages</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<!-- Small boxes (Stat box) -->
		<div class="box box-info">
			<div class="box-header with-border">
				<h3 class="box-title">
					<?php if (isset($status)): ?>
						<?= $status; ?>
					<?php endif; ?>
				</h3>
			</div><!-- /.box-header -->
			<!-- form start -->
			<form class="form-horizontal" method="post" enctype="multipart/form-data">
				<div class="box-body">
					<div class="form-group">
						<label for="inputEmail3" class="col-sm-2 control-label">Page Title*</label>
						<div class="col-sm-10">
							<input type="text" name="title" class="form-control" value="<?php if (isset($page['title'])) echo htmlspecialchars(trim($page['title'])); ?>" placeholder="Page Title" required autofocus />
						</div>
					</div>
					<?php foreach ($langs as $lang): ?>
						<div class="form-group">
							<label for="inputEmail3" class="col-sm-2 control-label">Page Title <?= $lang ?>*</label>
							<div class="col-sm-10">
								<input type="text" name="title_<?= $lang ?>" class="form-control" value="<?php if (isset($page['title_'.$lang])) echo htmlspecialchars(trim($page['title_'.$lang])); ?>" placeholder="Page Title <?= $lang ?>" required autofocus />
							</div>
						</div>
					<?php endforeach; ?>
					<div class="form-group">
						<label for="inputEmail3" class="col-sm-2 control-label">Brief*</label>
						<div class="col-sm-10">
							<textarea class="form-control" name="brief" rows="3" placeholder="Brief description" required><?php if (isset($page['brief'])) echo htmlspecialchars(trim($page['brief'])); ?></textarea>
						</div>
					</div>
					<?php foreach ($langs as $lang): ?>
						<div class="form-group">
							<label for="inputEmail3" class="col-sm-2 control-label">Brief <?= $lang ?>*</label>
							<div class="col-sm-10">
								<textarea class="form-control" name="brief_<?= $lang ?>" rows="3" placeholder="Brief description <?= $lang ?>" required><?php if (isset($page['brief_'.$lang])) echo htmlspecialchars(trim($page['brief_'.$lang])); ?></textarea>
							</div>
						</div>
					<?php endforeach; ?>
					<div class="form-group">
						<label for="inputEmail3" class="col-sm-2 control-label">Content*</label>
						<div class="col-sm-10">
							<div class="box-body pad">
										<textarea id="content" name="content" rows="10" cols="80">
																<?php if (isset($page['content'])) echo htmlspecialchars_decode(trim($page['content'])); else echo 'Page content here..' ?>
										</textarea>
							</div>
						</div>
					</div>
					<?php $i=2; ?>
					<?php foreach ($langs as $lang): ?>
						<div class="form-group">
							<label for="inputEmail3" class="col-sm-2 control-label">Content <?= $lang ?>*</label>
							<div class="col-sm-10">
								<div class="box-body pad">
										<textarea id="content_<?= $lang ?>" name="content_<?= $lang ?>" rows="10" cols="80">
																<?php if (isset($page['content_'.$lang])) echo htmlspecialchars_decode(trim($page['content_'.$lang])); else echo 'Page content here..' ?>
										</textarea>
								</div>
							</div>
						</div>
						<?php $i++; endforeach; ?>

					<div class="form-group">
						<label for="inputEmail3" class="col-sm-2 control-label">Sub of</label>
						<div class="col-sm-10">
							<select name="subof" class="form-control">
								<option value="">none</option>
								<?php foreach ($pages as $other_page):?>
									<option value="<?= $other_page['id'] ?>" <?php if(isset($page['subof']) && $page['subof'] == $other_page['id']) echo "selected" ?>><?= $other_page['title']?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="inputEmail3" class="col-sm-2 control-label">Meta description*</label>
						<div class="col-sm-10">
							<textarea class="form-control" name="meta_desc" rows="3" placeholder="Meta description" required><?php if (isset($page['meta_desc'])) echo htmlspecialchars(trim($page['meta_desc'])); ?></textarea>
						</div>
					</div>
					<?php foreach ($langs as $lang): ?>
						<div class="form-group">
							<label for="inputEmail3" class="col-sm-2 control-label">Meta description <?= $lang ?>*</label>
							<div class="col-sm-10">
								<textarea class="form-control" name="meta_desc_<?= $lang ?>" rows="3" placeholder="Meta description <?= $lang ?>" required><?php if (isset($page['meta_desc_'.$lang])) echo htmlspecialchars(trim($page['meta_desc_'.$lang])); ?></textarea>
							</div>
						</div>
					<?php endforeach; ?>
					<div class="form-group">
						<label for="inputEmail3" class="col-sm-2 control-label">Meta Keywords*</label>
						<div class="col-sm-10">
							<textarea class="form-control" name="meta_keywords" rows="3" placeholder="Meta keywords" required><?php if (isset($page['meta_keywords'])) echo htmlspecialchars(trim($page['meta_keywords'])); ?></textarea>
						</div>
					</div>
					<?php foreach ($langs as $lang): ?>
						<div class="form-group">
							<label for="inputEmail3" class="col-sm-2 control-label">Meta Keywords <?= $lang ?>*</label>
							<div class="col-sm-10">
								<textarea class="form-control" name="meta_keywords_<?= $lang ?>" rows="3" placeholder="Meta Keywords <?= $lang ?>" required><?php if (isset($page['meta_keywords_'.$lang])) echo htmlspecialchars(trim($page['meta_keywords_'.$lang])); ?></textarea>
							</div>
						</div>
					<?php endforeach; ?>
					<div class="form-group">
						<div class="col-sm-2">
						</div>
						<div class="col-sm-6">
							<div class="checkbox">
								<label>
									<input type="checkbox" value="1" name="form" <?php if(isset($page['form']) && $page['form'] == 1) echo "checked" ?>>
									Contains form
								</label>
							</div>

							<div class="checkbox">
								<label>
									<input type="checkbox" value="1" name="toplinks" <?php if(isset($page['toplinks']) && $page['toplinks'] == 1) echo "checked" ?>>
									Contains toplinks
								</label>
							</div>

							<div class="checkbox">
								<label>
									<input type="checkbox"  value="1" name="sidebar" <?php if(isset($page['sidebar']) && $page['sidebar'] == 1) echo "checked" ?>>
									Contains sidebar
								</label>
							</div>

							<div class="checkbox">
								<label>
									<input type="checkbox"  value="1" name="menu" <?php if(isset($page['menu']) && $page['menu'] == 1) echo "checked" ?>>
									Contains menu
								</label>
							</div>

							<div class="checkbox">
								<label>
									<input type="checkbox"  value="1" name="categories" <?php if(isset($page['categories']) && $page['categories'] == 1) echo "checked" ?>>
									Contains categories
								</label>
							</div>

							<div class="checkbox">
								<label>
									<input type="checkbox"  value="1" name="published" <?php if(isset($page['published']) && $page['published'] == 1) echo "checked" ?>>
									Published
								</label>
							</div>
						</div>
					</div>
				</div><!-- /.box-body -->
				<div class="box-footer">
					<button type="submit" name="submit" class="btn btn-info pull-right">Save Page</button>
				</div><!-- /.box-footer -->
			</form>
		</div>
		<!-- Main row -->


	</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<?php $this->load->view('footer'); ?>
<!-- CK Editor -->
<script src="https://cdn.ckeditor.com/4.4.3/standard/ckeditor.js"></script>
<script>
	$(function () {
		// Replace the <textarea id="editor1"> with a CKEditor
		// instance, using default configuration.
		CKEDITOR.replace('content');
	});
</script>
<?php foreach ($langs as $lang): ?>
	<script>
		$(function () {
			// Replace the <textarea id="editor1"> with a CKEditor
			// instance, using default configuration.
			CKEDITOR.replace('content_<?= $lang ?>');
		});
	</script>
<?php endforeach; ?>
