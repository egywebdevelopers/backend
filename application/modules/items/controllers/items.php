<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Items extends MX_Controller {

	public function __construct()
    {
        parent::__construct();
		$this->deny->deny_if_logged_out();
		
		$this->load->model("items_model");
		$this->load->model("pages/pages_model");
		$this->load->helper("img_processing");
    }
	
	
	public function index()
	{
		$data = array();

		$current_page = (int) $this->uri->segment(2);
		$per_page = 20;
		$users_count = $this->common_model->get_table_rows_count("pages");

		$config["base_url"] = site_url() . "users/";
		$config['uri_segment'] = 20;
		$config["total_rows"] = $users_count;
		$config["per_page"] = $per_page;
		$config['full_tag_open'] = "<ul class='pagination'>";
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";

		$this->pagination->initialize($config);

		$pages = $this->pages_model->get_pages_pagination($current_page, $per_page);
		if ($pages)
		{
			$data["pages"] = $pages;
			$data["pagination"] = $this->pagination->create_links();
		}

		if (isset($_POST["submit"]))
		{
			$query = htmlspecialchars(trim($_POST["search"]));
			redirect(site_url() . "users/search/$query");
		}

		$this->load->view("manage_pages_view", $data);
    }

	public function page($id = "")
	{
		$page = $this->common_model->get_subject_with_token("pages", "id", $id);
		if (empty($id) OR ! $page) redirect(site_url() . "pages");
		$data = array();

		$current_page = (int) $this->uri->segment(2);
		$per_page = 20;
		$users_count = $this->common_model->get_table_rows_count("pages");

		$config["base_url"] = site_url() . "users/";
		$config['uri_segment'] = 20;
		$config["total_rows"] = $users_count;
		$config["per_page"] = $per_page;
		$config['full_tag_open'] = "<ul class='pagination'>";
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";

		$this->pagination->initialize($config);

		$items = $this->items_model->get_items_pagination($current_page, $per_page, $id);
		if ($items)
		{
			$data["items"] = $items;
			$data["pagination"] = $this->pagination->create_links();
		}

		if (isset($_POST["submit"]))
		{
			$query = htmlspecialchars(trim($_POST["search"]));
			redirect(site_url() . "users/search/$query");
		}

		$this->load->view("manage_items_view", $data);
	}

	public function search($query = "")
	{
		$authorized = $this->common_model->authorized_to_view_page("manage_users");
		if ($authorized)
		{
			if (empty($query)) redirect(site_url() . "users");
			$query = urldecode($query);
			
			$data = array();
			
			$current_page = (int) $this->uri->segment(4);
			$per_page = 1;
			$users_count = $this->users_model->get_search_rows_count($query);
			
			$config["base_url"] = site_url() . "users/search/$query";
			$config['uri_segment'] = 4;
			$config["total_rows"] = $users_count;
			$config["per_page"] = $per_page;
			$this->pagination->initialize($config);
			
			$users = $this->users_model->search_users($query, $current_page, $per_page);
			if ($users)
			{
				$data["users"] = $users;
				$data["pagination"] = $this->pagination->create_links();
			}
			
			if (isset($_POST["submit"]))
			{
				$query = htmlspecialchars(trim($_POST["search"]));
				redirect(site_url() . "users/search/$query");
			}
			
			$this->load->view("manage_users_view", $data);
		}
	}
	
	
	public function add()
	{
		$data = array();
		$langs = $this->pages_model->get_langs();
		$langs_arr = explode(',', $langs['value']);
		unset($langs_arr[0]);
		$data['langs'] = $langs_arr;

		$data['pages'] = $this->pages_model->get_pages();

		if (isset($_POST["submit"]))
		{
			$title = htmlspecialchars(trim($_POST["title"]));
			$paragraph = htmlspecialchars(trim($_POST["paragraph"]));
			$page_id = $_POST['page'];
			$image = @$_FILES["image"]["name"];
			$active = (isset($_POST['active']) && $_POST['active'] == 1) ? 1 : 0 ;

			$lang_data = array();
			foreach ($langs_arr as $lang)
			{
				$lang_data['title_'.$lang] = htmlspecialchars(trim($_POST["title_".$lang]));
				$lang_data['paragraph_'.$lang] = htmlspecialchars(trim($_POST["paragraph_".$lang]));
			}

			if (empty($title) OR empty($paragraph) OR empty($page_id))
			{
				$data["status"] = "<p class='error-msg'>Please fill in required fields(*)</p>";
			}
			else
			{
				if ( ! empty($_FILES["image"]["name"]))
				{
					$tmp_name = $_FILES['image']['tmp_name']; // getting the temporary file name
					$allowed_exts = array('jpg', 'JPG', 'jpeg', 'JPEG', 'png', 'PNG', 'gif', 'GIF'); // specifying the allowed extensions
					$a = explode('.', $image);
					$file_ext = strtolower(end($a)); unset($a);
					$path = ITEM_IMAGES_PATH; // folder we store the employees photos in

					// Restricting file uploading to image files only
					if ( ! in_array($file_ext, $allowed_exts))
					{
						$data["status"] = "<p class='error-msg'>This file extension is not an image!</p>";
					}
					else
					{
						// Success. Give the picture a unique name and upload it
						$picture_name = time() . "." . $file_ext;
						move_uploaded_file($tmp_name, $path . $picture_name);
						$this->items_model->insert_item($lang_data, $title, $paragraph, $active, $page_id, $picture_name);
						$this->session->set_flashdata("status", "Added successfully.");
						redirect(site_url() . "pages");
					}
				}
				else
				{
					// Insert information into database
					$this->items_model->insert_item($lang_data, $title, $paragraph, $active, $page_id);

					$this->session->set_flashdata("status", "Added successfully.");
					redirect(site_url() . "pages");
				}
			}
		}

		$this->load->view("add_item_view", $data);

	}

	public function edit($id = "")
	{
		$data = array();
		$item = $this->common_model->get_subject_with_token("items", "id", $id);
		if (empty($id) OR ! $item) redirect(site_url() . "pages");

		$data['item'] = $item;
		$langs = $this->pages_model->get_langs();
		$langs_arr = explode(',', $langs['value']);
		unset($langs_arr[0]);
		$data['langs'] = $langs_arr;

		$data['pages'] = $this->pages_model->get_pages();

		if (isset($_POST["submit"]))
		{
			$title = htmlspecialchars(trim($_POST["title"]));
			$paragraph = htmlspecialchars(trim($_POST["paragraph"]));
			$page_id = $_POST['page'];
			$image = @$_FILES["image"]["name"];
			$active = (isset($_POST['active']) && $_POST['active'] == 1) ? 1 : 0 ;

			$lang_data = array();
			foreach ($langs_arr as $lang)
			{
				$lang_data['title_'.$lang] = htmlspecialchars(trim($_POST["title_".$lang]));
				$lang_data['paragraph_'.$lang] = htmlspecialchars(trim($_POST["paragraph_".$lang]));
			}

			if (empty($title) OR empty($paragraph) OR empty($page_id))
			{
				$data["status"] = "<p class='error-msg'>Please fill in required fields(*)</p>";
			}
			else
			{
				if ( ! empty($_FILES["image"]["name"]))
				{
					$tmp_name = $_FILES['image']['tmp_name']; // getting the temporary file name
					$allowed_exts = array('jpg', 'JPG', 'jpeg', 'JPEG', 'png', 'PNG', 'gif', 'GIF'); // specifying the allowed extensions
					$a = explode('.', $image);
					$file_ext = strtolower(end($a)); unset($a);
					$path = ITEM_IMAGES_PATH; // folder we store the employees photos in

					// Restricting file uploading to image files only
					if ( ! in_array($file_ext, $allowed_exts))
					{
						$data["status"] = "<p class='error-msg'>This file extension is not an image!</p>";
					}
					else
					{
						// Success. Give the picture a unique name and upload it
						$picture_name = time() . "." . $file_ext;
						move_uploaded_file($tmp_name, $path . $picture_name);
						$this->items_model->update_item($id, $lang_data, $title, $paragraph, $active, $page_id, $picture_name);
						$this->session->set_flashdata("status", "Added successfully.");
						redirect($_SERVER['HTTP_REFERER']);
					}
				}
				else
				{
					// Insert information into database
					$this->items_model->update_item($id, $lang_data, $title, $paragraph, $active, $page_id);

					$this->session->set_flashdata("status", "Added successfully.");
					redirect($_SERVER['HTTP_REFERER']);
				}
			}
		}

		$this->load->view("edit_item_view", $data);

	}
    
	
	public function delete($id = "")
    {
		$item = $this->common_model->get_subject_with_token("items", "id", $id);
		if (empty($id) OR ! $item) redirect(site_url() . "pages");

		$this->common_model->delete_subject("items", "id", $id);

		$this->session->set_flashdata("status", "Item deleted successfully.");
		redirect($_SERVER['HTTP_REFERER']);
	}

}


/* End of file users.php */
/* Location: ./application/modules/users/controllers/users.php */