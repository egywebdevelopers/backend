<?php $this->load->view('header'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Dashboard
			<small>Add Item</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Items</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<!-- Small boxes (Stat box) -->
		<div class="box box-info">
			<div class="box-header with-border">
				<h3 class="box-title">
					<?php if (isset($status)): ?>
						<?= $status; ?>
					<?php endif; ?>
				</h3>
			</div><!-- /.box-header -->
			<!-- form start -->
			<form class="form-horizontal" method="post" enctype="multipart/form-data">
				<div class="box-body">
					<div class="form-group">
						<label for="inputEmail3" class="col-sm-2 control-label">Page*</label>
						<div class="col-sm-10">
							<select name="page" class="form-control" required>
								<option value="">Select page</option>
								<?php foreach ($pages as $page):?>
									<option value="<?= $page['ID'] ?>" <?php if(isset($_POST['page']) && $_POST['page'] == $page['ID']) echo "selected" ?>><?= $page['title']?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="inputEmail3" class="col-sm-2 control-label">Item Title*</label>
						<div class="col-sm-10">
							<input type="text" name="title" class="form-control" value="<?php if (isset($_POST['title'])) echo htmlspecialchars(trim($_POST['title'])); ?>" placeholder="Page Title" required autofocus />
						</div>
					</div>
					<?php foreach ($langs as $lang): ?>
						<div class="form-group">
							<label for="inputEmail3" class="col-sm-2 control-label">Item Title <?= $lang ?>*</label>
							<div class="col-sm-10">
								<input type="text" name="title_<?= $lang ?>" class="form-control" value="<?php if (isset($_POST['title_'.$lang])) echo htmlspecialchars(trim($_POST['title_'.$lang])); ?>" placeholder="Page Title <?= $lang ?>" required autofocus />
							</div>
						</div>
					<?php endforeach; ?>
					<div class="form-group">
						<label for="inputEmail3" class="col-sm-2 control-label">Paragraph*</label>
						<div class="col-sm-10">
							<textarea class="form-control" name="paragraph" rows="3" placeholder="Paragraph.." required><?php if (isset($_POST['paragraph'])) echo htmlspecialchars(trim($_POST['paragraph'])); ?></textarea>
						</div>
					</div>
					<?php foreach ($langs as $lang): ?>
						<div class="form-group">
							<label for="inputEmail3" class="col-sm-2 control-label">Paragraph <?= $lang ?>*</label>
							<div class="col-sm-10">
								<textarea class="form-control" name="paragraph_<?= $lang ?>" rows="3" placeholder="Paragraph.. <?= $lang ?>" required><?php if (isset($_POST['paragraph_'.$lang])) echo htmlspecialchars(trim($_POST['paragraph_'.$lang])); ?></textarea>
							</div>
						</div>
					<?php endforeach; ?>
					<div class="form-group">
						<label for="inputEmail3" class="col-sm-2 control-label">Image</label>
						<div class="col-sm-10">
							<input type="file" name="image" class="form-control" autofocus />
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-2">
						</div>
						<div class="col-sm-6">

							<div class="checkbox">
								<label>
									<input type="checkbox"  value="1" name="active" <?php if(isset($_POST['active']) && $_POST['active'] == 1) echo "checked" ?>>
									Active
								</label>
							</div>
						</div>
					</div>
				</div><!-- /.box-body -->
				<div class="box-footer">
					<button type="submit" name="submit" class="btn btn-info pull-right">Add Item</button>
				</div><!-- /.box-footer -->
			</form>
		</div>
		<!-- Main row -->


	</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<?php $this->load->view('footer'); ?>

