<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Items_model extends CI_Model {
    
    public function __construct()
    {
        parent::__construct();
    }



    public function insert_item($lang_data, $title, $paragraph, $active, $page_id, $image = '')
    {
        if ($image != '')
        {
            $arr = array('title' => $title, 'paragraph' => $paragraph, 'pageID' => $page_id, 'active' => $active, 'image' => $image);
        }
        else
        {
            $arr = array('title' => $title, 'paragraph' => $paragraph, 'active' => $active, 'pageID' => $page_id);
        }

        $data_arr =  array_merge($arr, $lang_data);
        $this->db->insert('items', $data_arr);
    }

    public function update_item($id, $lang_data, $title, $paragraph, $active, $page_id, $image = '')
    {
        if ($image != '')
        {
            $arr = array('title' => $title, 'paragraph' => $paragraph, 'active' => $active, 'pageID' => $page_id, 'image' => $image);
        }
        else
        {
            $arr = array('title' => $title, 'paragraph' => $paragraph, 'active' => $active, 'pageID' => $page_id);
        }
        $data_arr =  array_merge($arr, $lang_data);
        $this->db->where('ID', $id);
        $this->db->update('items', $data_arr);
    }
    
    public function get_items_pagination($offset, $limit, $page_id)
    {
        $this->db->where('pageID', $page_id);
        $this->db->order_by('ID', 'desc');
        $query = $this->db->get('items');
        return $query->result_array();
    }

    public function get_item_pagination($offset, $limit, $page_id)
    {
        $this->db->where('pageID', $page_id);
        $this->db->order_by('ID', 'desc');
        $query = $this->db->get('items');
        return $query->result_array();
    }

    
}


/* End of file users_model.php */
/* Location: ./application/modules/users/models/users_model.php */