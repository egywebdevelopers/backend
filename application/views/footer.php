<footer class="main-footer">

    <strong>Copyright &copy; 2016 <a href="http://egyweb.com">EGYWEB</a>.</strong> All rights reserved.
</footer>

<!-- Control Sidebar -->

<!-- Add the sidebar's background. This div must be placed
     immediately after the control sidebar -->
<div class="control-sidebar-bg"></div>
</div><!-- ./wrapper -->

<!-- jQuery 2.1.4 -->
<script src="<?= ASSETS ?>js/jQuery-2.1.4.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.5 -->
<script src="<?= ASSETS ?>js/bootstrap.min.js"></script>
<!-- Sparkline -->
<script src="<?= ASSETS ?>plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?= ASSETS ?>plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?= ASSETS ?>plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?= ASSETS ?>plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src="<?= ASSETS ?>plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?= ASSETS ?>plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?= ASSETS ?>plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="<?= ASSETS ?>plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?= ASSETS ?>plugins/fastclick/fastclick.min.js"></script>
<!-- AdminLTE App -->
<script src="<?= ASSETS ?>js/app.min.js"></script>
<script src="<?= ASSETS; ?>js/sweetalert.min.js"></script>

<script src="<?= ASSETS; ?>js/jquery.Jcrop.min.js"></script>
<script src="<?= ASSETS; ?>js/template_Jcrop_script.js"></script>
<script>
    function viewInModal(ajaxURL) {
        var targetURL = "<?= site_url(); ?>" + ajaxURL;
        $(".modal-content").html("");
        $(".modal-content").load(targetURL);
    }

    // Uses a sweetalert to ask for user confirmation that they want to delete something
    function alertDelete(ajaxURL, message) {
        var targetURL = "<?= site_url(); ?>" + ajaxURL;
        swal({
                title: "Alert!",
                text: message,
                type: "warning",
                allowOutsideClick: true,
                showCancelButton: true,
                confirmButtonColor: "#C9302C",
                confirmButtonText: "Yes",
                cancelButtonText: "No",
                closeOnConfirm: false
            },
            function () {
                window.location.href = targetURL;
            }
        );
    }
</script>
</body>
</html>
